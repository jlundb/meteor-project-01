Package.on_use(function (api) {
  api.use(["tap:i18n@1.8.2"], ["client", "server"]);
  api.add_files("package-tap.i18n", ["client", "server"]);

  api.add_files([
    "../../i18n/en.i18n.json",
    "../../i18n/no.i18n.json"
  ], ["client", "server"]);
});
