import { Meteor } from 'meteor/meteor';
import '../imports/api/articles.js';
import '../imports/api/article_images.js';
import '../imports/api/category_options.js';
import '../imports/api/carousel_images.js';
import '../imports/api/company_info.js';
import '../imports/api/gallery_images.js';
import '../imports/api/images.js';
import '../imports/api/languages.js';
import '../imports/api/routes.js';
import '../imports/api/usermessages.js';
import '../imports/startup/accounts-config.js';

Meteor.startup(() => {

});

Meteor.methods({
  'isLoggedIn': () => {
    return Meteor.userId() ? true : false;
  },
});
