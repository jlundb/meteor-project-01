//JQUERY UPLOAD
UploadServer.init({
 tmpDir: process.env.PWD + '',
 uploadDir: process.env.PWD + '',
 checkCreateDirectories: true,
 minFileSize: 1,
 maxFileSize: 10000000000,
 cacheTime: 100,
 overwrite: false,
 mimeTypes: { "xml": "application/xml","vcf": "text/x-vcard"},
 getDirectory: function(fileInfo, formData) {
   const directoryName = formData.directoryName;
   const subDirectory = formData.subDirectory;
   return directoryName + '/';
 },
 getFileName: function(fileInfo, formData) {
   const fileName = new Date().getTime() + fileInfo.name;
   fileInfo.formData = formData;
   fileInfo.fullName = formData.directoryName + fileName;
   fileInfo.imageName = formData.imageName;
   fileInfo.imageDescription = formData.imageDescription;
   fileInfo.category = formData.category;

   return fileName;
 }

});
