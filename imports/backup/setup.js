db.routes.insert([
  {"text" : "main_page", "link":"/", "order": 0, 'template':'home', 'visibleInMenu':true},
  {"text" : "about_us", "link":"/about", "order": 1, 'template':'about', 'visibleInMenu':true},
  {"text" : "news", "link":"/news", "order": 2, 'template':'news', 'visibleInMenu':true},
  {"text" : "gallery", "link":"/gallery", "order": 3, 'template':'gallery', 'visibleInMenu':true},
  {"text" : "contact", "link":"/contact", "order": 4, 'template':'contact', 'visibleInMenu':true},
  {"text" : "login", "link":"/login", "order": 5, 'template':'korntinLogin', 'visibleInMenu':false}
]);

db.category_options.insert([
  {'value':'kitchen', 'text':'kitchen', 'order': 1, 'selected':''},
  {'value':'living', 'text':'living', 'order':2, 'selected':'selected', 'checked':'checked'},
  {'value':'bed', 'text':'bed', 'order':3, 'selected':''},
]);

db.languages.insert([
  {'language':'norsk', 'code':'no', 'standard':'ISO 639-1', 'icon':'images/nationalities/norwegian.png'},
  {'language':'english', 'code':'en', 'standard':'ISO 639-1', 'icon':'images/nationalities/english.png'}
]);

db.articles.insert([{
  'title':'Om oss',
  'body': '<p> Vi har spesialisert oss på håndlagde kvalitetsmøbler med utgangspunkt i den Preussiske møbeltradisjon. Utført i solid eik og med alt av utskjæringer og snekkerarbeide gjort for hånd på gamlemåten. Vi leverer spisestuer, kjøkken, skjenker, buffeer, brystningspaneler, spesialdesignede dører, innebygde akvarier og ellers det kunden har som ønske.</p> <p> Kundene våre kommer ofte også med egne ideer og tegninger. I samarbeid med våre dyktige møbeldesignere og snekkere lager vi et ferdig produkt med et personlig preg. </p> <p> Mange kunder vil f.eks. gjerne ha egne monogram eller årstall skåret inn. Vi tilbyr et standard utvalg av farger og finish, men andre farger og utførelser er også mulig. </p> <p> Fabrikken vår har levert møbler til alt fra hus, hytter, leiligheter, restauranter, hoteller, offentlige bygninger og ambassader. Et av de mer spesielle oppdragene var til pave Johannes Paulus. Nå ser vi frem til å kunne hjelpe deg med akkurat dine ønsker. </p> <p> Ta kontakt i dag for en hyggelig og uforpliktende samtale. </p>',
  'active':true,
  'function':'about',
  'language':'no',
  'order':0
},
{
  'title':'About us',
  'body': '<p> We have specialized in quality furniture </p>',
  'active':true,
  'function':'about',
  'language':'en',
  'order':0
}
]);

db.articles.insert([{
  'title':'VELKOMMEN TIL KORNTIN MØBLER',
  'body': '<p>Velkommen til Korntin Møbler. Vi produserer klassiske håndlagde møbler,utformet i preusserstil.</p><p>Møblene kan tilpasses hver enkelt kundes egne ønsker og behov. Alle møbler produseres i Polen og utformes i massiv eik uten spon. Alle elementer utføres etter høye kvalitetsstandarder.</p> <p>Ta gjerne kontakt i dag for mer informasjon om hvilke løsninger vi kan tilby.</p>',
  'active':true,
  'function':'home',
  'language':'no',
  'order':0
},
{
  'title':'WELCOME TO KORNTIN',
  'body': '<p>Velkommen til Korntin Møbler. Vi produserer klassiske håndlagde møbler,utformet i preusserstil.</p><p>Møblene kan tilpasses hver enkelt kundes egne ønsker og behov. Alle møbler produseres i Polen og utformes i massiv eik uten spon. Alle elementer utføres etter høye kvalitetsstandarder.</p> <p>Ta gjerne kontakt i dag for mer informasjon om hvilke løsninger vi kan tilby.</p>',
  'active':true,
  'function':'home',
  'language':'en',
  'order':0
},
{
  'title':'VI TILBYR',
  'body': '<ul><li>Håndlagde eikemøbler i preusserstil</li><li>Møbelutformingen kan tilpasses etter hver enkelt kundes behov</li><li>Se vårt bildegalleri for noen eksempler på produkter vi har levert</li><li>Møblene produseres i massiv eik og etter høy standard</li></ul>',
  'active':true,
  'function':'home',
  'language':'no',
  'order':0
},
{
  'title':'WE OFFER',
  'body': '<ul><li>Håndlagde eikemøbler i preusserstil</li><li>Møbelutformingen kan tilpasses etter hver enkelt kundes behov</li><li>Se vårt bildegalleri for noen eksempler på produkter vi har levert</li><li>Møblene produseres i massiv eik og etter høy standard</li></ul>',
  'active':true,
  'function':'home',
  'language':'en',
  'order':0
}
]);

db.article_images.insert([
  {'function':'about'}
]);

db.company_info.insert({
  'name':'korntin',
  'postalCode': '3267',
  'city':'Larvik',
  'address':'Holmejordetveien 32',
  'phone':'909 101 98',
  'mail':'info@korntin.no'
});

db.createCollection("company_info", {"autoIndexID":true});
