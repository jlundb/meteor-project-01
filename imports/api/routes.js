import { Mongo } from 'meteor/mongo';

export const Routes = new Mongo.Collection('routes', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish("routes", function() {
    return Routes.find({});
  });
}

Meteor.methods({
  'routes.insert':function(route) {

  },
  'routes.update':function(route)  {

  },
  'routes.remove':function(route) {

  },
});
