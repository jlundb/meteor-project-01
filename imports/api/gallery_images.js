import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { Match } from 'meteor/check';
import { Images } from './images.js';

export const GalleryImages = new Mongo.Collection('gallery_images', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish("gallery_images", function() {
    return GalleryImages.find({});
  });
}

const GalleryImageSchema = new SimpleSchema({
  'encoding': {
    'type':String,
    'max': 4194310,
    'min': 10
  },
  'imageName':{
    'type':String,
    'max': 50
  },
  'imageDescription':{
    'type':String,
    'max':500,
    'optional':true
  },
  'category':{
    'type':String
  },
  'userId':{
    'type':String,
    'optional':true
  },
  'insertedDate':{
    'type':Date,
    'optional':true
  }
});

Meteor.methods({
  'galleryImages.insert':(galleryImage, image) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');

    Meteor.call('images.insert', image, (error, result) => {
      if(error) throw new Meteor.error('Error during image insert "images.insert" method');
      if(result) {
          const imageId = result;
          galleryImage.imageId = imageId;
          galleryImage.insertedDate = new Date();
          return GalleryImages.insert(galleryImage);
      }
    });
  },
  'galleryImages.simpleInsert':(galleryImage) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
        galleryImage.userId = Meteor.userId()._str;
        galleryImage.insertedDate = new Date();
        check(galleryImage, GalleryImageSchema);
        return GalleryImages.insert(galleryImage);
  },
  'galleryImages.remove':(galleryImageId) => {
      if(!Meteor.userId()) throw new Meteor.error('Not logged in');
      return GalleryImages.remove({_id:galleryImageId});
  },
  'galleryImages.find': (selector, options) => {
    return GalleryImages.find(selector, options).fetch();

  },
  'galleryImages.count':(selector) => {
    return GalleryImages.find(selector).count();
  }
});
