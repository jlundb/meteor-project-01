import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

export const CompanyInfo = new Mongo.Collection('company_info', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish('company_info', function() {
    return CompanyInfo.find({'name':'korntin'});
  });
}

const CompanyInfoSchema = new SimpleSchema({
  '_id': {
    'type':SimpleSchema.RegEx.Id
  },
  'name':{
    'type':String,
    'min':1,
    'max':100
  },
  'postalCode':{
    'type':String,
    'min':4,
    'max': 12,
    'optional':true
  },
  'city':{
    'type':String,
    'min':2,
    'max':100,
    'optional':true
  },
  'address':{
    'type':String,
    'min':2,
    'max':250,
    'optional':true
  },
  'phone':{
    'type':String,
    'min':9,
    'max':50,
    'optional':true
  },
  'mail':{
    'type':String,
    'max':256,
    'min':10,
    'optional':true
  }
});

Meteor.methods({
  'CompanyInfo.update':(modifier) => {
        if(!Meteor.userId()) throw new Meteor.error('Not logged in');
        check(modifier,CompanyInfoSchema);
        const selector = {'name':'korntin'};
        return CompanyInfo.update(selector, modifier);
  },
});
