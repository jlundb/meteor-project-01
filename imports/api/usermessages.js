import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

export const UserMessages = new Mongo.Collection('usermessages', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish("usermessages", function() {
    if(this.userId) return UserMessages.find({});
  });
}

const UserMessageSchema = new SimpleSchema({
  'name': {
    'type':String,
    'max':50
  },
  'mail': {
    'type':String,
    'max':254,
    'regEx':SimpleSchema.RegEx.Email,
    'optional':true
  },
  'phone': {
    'type':String,
    'max':20,
    'optional':true
  },
  'message': {
    'type':String,
    'max':1000,
  },
  'topic': {
    'type':String,
    'max': 100,
  },
  'submittedAt': {
    'type':Date,
    'optional':true
  }

});

Meteor.methods({
  'UserMessages.insert': (userMessage) => {
    userMessage.submittedAt = new Date();
    check(userMessage, UserMessageSchema);
    return UserMessages.insert(userMessage);
  },
  'UserMessages.remove': (userMessageId) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    UserMessages.remove({'_id':userMessageId});
  },
  'UserMessages.count': (filter) => {
    check(filter, Object);
    if(!Meteor.userId()) return 0;
    else return UserMessages.find(filter).count();
  }
});
