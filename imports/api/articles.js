import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

export const Articles = new Mongo.Collection('articles', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish("articles", function(args, lang) {
    let filter = {'function': args};
    if(lang) filter.language = lang;
    if(!this.userId) filter.active = true;
    return Articles.find(filter);
  });
}

const ArticlesSchema = new SimpleSchema({
  'title': {
    'type':String,
    'min':1,
    'max':50
  },
  'body': {
    'type':String
  },
  'active': {
    'type':Boolean
  },
  'insertedDate': {
    'type':Date,
    'optional':true
  },
  'userId': {
    'type':String,
    'optional':true
  },
  'function': {
    'type':String,
    'min':2,
    'max':100
  },
  'language': {
    'type':String,
    'min':2,
    'max':2
  },
  'order': {
    'type':Number
  },
  'updatedDate': {
    'type':Date,
    'optional': true
  }

});

Meteor.methods({
  'Articles.insert': (article) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    article.userId = Meteor.userId();
    article.insertedDate = new Date();
    check(article, ArticlesSchema);
    return Articles.insert(article);
  },
  'Articles.update': (articleId, article) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    check(article, ArticlesSchema);
    article.updatedDate = new Date();
    Articles.update({'_id':articleId},{'$set':article});
  },
  'Articles.remove': (articleId) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    Articles.remove({'_id':articleId});
  },
  'Articles.count': (filter) => {
    return Articles.find(filter).count();
  },
});
