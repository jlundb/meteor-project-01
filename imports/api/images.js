import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { Match } from 'meteor/check';
import { check } from 'meteor/check';

export const Images = new Mongo.Collection('images', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
    Meteor.publish("images", function() {
      return Images.find({});
    });
}

const ImageSchema = new SimpleSchema({
  'name': {
    'type':String,
    'max':200,
    'min':1
  },
  'desc': {
    'type':String,
    'max':1000,
    'optional':true
  },
  'insertedDate': {
    'type':Date,
    'optional':true
  },
  'alt': {
    'type':String,
    'optional':true,
    'max':200
  },
  'encoding': {
    'type':String,
    'max': 4194310,
    'min': 10
  }
});

Meteor.methods({
  'images.insert':(image) => {
    image.insertedDate = new Date();
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    check(image, ImageSchema);
    return Images.insert(image);

  },
  'images.remove':(imageId) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    Images.remove({'_id':imageId});

  },
  'images.count': () => {
    return Images.find({}).count();
  },
});
