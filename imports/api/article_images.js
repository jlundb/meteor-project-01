import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

export const ArticleImages = new Mongo.Collection('article_images', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish('article_images', function(arg) {
    const filter = {'function': arg};
    return ArticleImages.find({});
  });
}

const ArticleImagesSchema = new SimpleSchema({
  'function': {
    'type':String,
    'max':50
  },
  'insertedDate': {
    'type':Date,
    'optional':true
  },
  'updatedDate': {
    'type':Date,
    'optional':true
  },
  'userId': {
    'type':String,
    'optional':true
  }
});

Meteor.methods({
  'ArticleImages.insert':(articleImage) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    articleImage.userId = Meteor.userId();
    articleImage.insertedDate = new Date();
    check(article, ArticleImagesSchema);
    return ArticleImages.insert(articleImage);

  },
  'ArticlesImages.update':(articleImage) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    if(!articleImage.imageId) throw new Meteor.error('Image id missing');
    articleImage.updatedDate = new Date();
    ArticleImages.update({'_id':articleImage._id},{'$set': {'imageId': articleImage.imageId}});
  },
  'ArticlesImages.remove':(articleImageId) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    ArticleImages.remove({'_id':articleImageId});
  },
});
