import { Mongo } from 'meteor/mongo';

export const Languages = new Mongo.Collection('languages', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish("languages", function() {
    return Languages.find({});
  });
}
