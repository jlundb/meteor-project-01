import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

export const CategoryOptions = new Mongo.Collection('category_options', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish("category_options", function() {
    return CategoryOptions.find({});
  });
}

Meteor.methods({
  'categoryOption.find':() => {
        CategoryOptions.find({});
  },
});
