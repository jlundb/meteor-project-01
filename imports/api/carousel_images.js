import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

export const CarouselImages = new Mongo.Collection('carousel_images', {idGeneration: 'MONGO'});

if(Meteor.isServer) {
  Meteor.publish("carousel_images", function() {
    return CarouselImages.find({});
  });
}

const CarouselImageSchema = new SimpleSchema({
  'name': {
    'type':String,
    'max':50,
    'min':1
  },
  'slideNr': {
    'type':Number,
    'min': 0,
    'max': 10
  },
  'encoding': {
    'type':String,
    'max': 4194310,
    'min': 10
  },
  'insertedDate': {
    'type':Date,
    'optional':true
  },
});

Meteor.methods({
  'CarouselImages.insert': (carouselImage) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    carouselImage.insertedDate = new Date();
    check(carouselImage, CarouselImageSchema);
    return CarouselImages.insert(carouselImage);

  },
  'CarouselImages.update':(selector, modifier, multi) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
      console.log(selector);
      console.log(modifier);
      console.log(multi);
      return CarouselImages.update(selector, modifier, multi);
  },
  'CarouselImages.remove': (carouselImage) => {
      if(!Meteor.userId()) throw new Meteor.error('Not logged in');
      const selector = {'slideNr':{'$gt': carouselImage.slideNr}};
      const modifier = {'$inc':{'slideNr': -1}};
      const multi = {'multi': true};

      CarouselImages.remove({'_id':carouselImage._id}, (err, res) => {
        if(err) {
          console.log(err);
        } else {
          Meteor.call('CarouselImages.update', selector, modifier, multi, (err) => {
            if(err) console.log(err);
          });
        }
      });
  },
  'CarouselImages.swapPosition': (carouselImage1, carouselImage2) => {
    if(!Meteor.userId()) throw new Meteor.error('Not logged in');
    const selector1 = {'_id': carouselImage1._id};
    const modifier1 = {'$set':{'slideNr': carouselImage2.slideNr}};
    const selector2 = {'_id': carouselImage2._id};
    const modifier2 = {'$set':{'slideNr': carouselImage1.slideNr}};
    const multi = {'multi':false};
    Meteor.call('CarouselImages.update', selector1, modifier1, multi, (err, res) => {
      if(err) {
        console.log(err);
      } else if(!res || res !== 1) {
        console.log('There was an error: ', res);
      } else {
        Meteor.call('CarouselImages.update', selector2, modifier2, multi, (err, res) => {
          if(err || !res || res !== 1) console.log('Failed to update second image');
        });
      }
    });
  },
  'CarouselImages.count':() => {
    return CarouselImages.find({}).count();
  },
  'CarouselImages.maxSlideNr':() => {
    const selector = {};
    const options = {'sort':{'slideNr':-1}, 'limit':1};
    const highestSlideImg = CarouselImages.find(selector, options).fetch();
    return (highestSlideImg.length > 0) ? highestSlideImg[0].slideNr : -1;
  },
});
