import { Template } from 'meteor/templating';
import { Routes} from '../../api/routes.js';
import { Languages } from '../../api/languages.js';
import { CompanyInfo } from '../../api/company_info.js';
import './header.html';

Template.korntinHeader.created = function() {
  Meteor.subscribe('routes');
  Meteor.subscribe('languages');
  Meteor.subscribe('company_info');
};

Template.korntinHeader.helpers({
  'routes': function() {
    return Routes.find({'visibleInMenu':true},{'sort':{'order':1}});
  },
  'languages': function() {
    return Languages.find({});
  },
  'currLang': function() {
    var langCode = TAPi18n.getLanguage();
    return Languages.findOne({'code':langCode});
  },
  'companyInfo':function() {
    return CompanyInfo.findOne({});
  }
});

Template.korntinHeader.events({
  'click .glyphicon-menu-hamburger': function(event, template) {
    if(template.$('#korntin-nav-menu').css('display') === 'none') {
      template.$('#korntin-nav-menu').show();
    } else {
      template.$('#korntin-nav-menu').delay('slow').hide();
    }
  },
  'click .korntin-nav-mobile-item': function(event, template) {
    var navItem = Blaze.getData(event.target);
    if(navItem && navItem.link) Router.go(navItem.link);
    Meteor.setTimeout(function() {
      template.$('#korntin-nav-menu').hide();
    }, 300);
  },
  'click .korntin-nav-std-item': function(event, template) {
      console.log("Standard nav event");
      var navItem = Blaze.getData(event.target);
      if(navItem && navItem.link) Router.go(navItem.link);
  
  },
  'click .mobile-header-text': function(event, template) {
    if(template.$('#korntin-nav-menu').css('display') !== 'none') {
      Meteor.setTimeout(function() {
        template.$('#korntin-nav-menu').hide();
      }, 300);
      Router.go('/');
    }
  },
  'click .korntin-header-text': function() {
    Router.go('/');
  },
  'click .header-lang-item': function(event) {
    var language = Blaze.getData(event.target);
    TAPi18n.setLanguage(language.code);
    if(document.body) document.body.scrollIntoView();
  },
  'click .mobile-lang': function(event, template) {
    Meteor.setTimeout(function() {
      template.$('#korntin-nav-menu').hide();
    }, 300)
  },
  'click #pc-login': function() {
    Router.go('/login');
  },
  'click #pc-logout': function() {
    Meteor.logout(function(err) {
      if(err) console.log('Logout failed');
      else Router.go('/');
    });
  }
});
