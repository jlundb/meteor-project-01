import { Template } from 'meteor/templating';


import './panels/main/carousel.js';
import './panels/main/home.js';
import './header/header.js';
import './footer/footer.js';
import './body.html';
import './layout.js';
