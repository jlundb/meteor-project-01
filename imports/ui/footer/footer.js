import { Template } from 'meteor/templating';
import { CompanyInfo } from '../../api/company_info.js';

import './footer.html';

Template.korntinFooter.helpers({
  'companyInfo':function() {
    return CompanyInfo.findOne({});
  }
});
