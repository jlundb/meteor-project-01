import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor'
import './login.html';


Template.korntinLogin.events({
  'submit .login-form': function(event, template) {
    var userData = {
        'email':event.target.email.value,
        'password':event.target.password.value
    };

    Meteor.loginWithPassword(userData.email, userData.password, function(err) {
      if(err) {
          event.target.email.value = '';
          event.target.password.value = '';
          Router.go('/login');
      } else {
          Router.go('/');
      }
    });
    return false;
  },
  'click #logout-button': function(event, template) {
    Meteor.logout(function(err) {
      if(err) console.log('Logout failed');
      else Router.go('/login');
    });
  }
});
