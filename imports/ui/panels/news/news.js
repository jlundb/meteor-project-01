import { Articles } from '../../../api/articles.js';
import { Blaze } from 'meteor/blaze';
import { Template } from 'meteor/templating';
import { Languages } from '../../../api/languages.js';
import { ReactiveVar } from 'meteor/reactive-var';
import './news.html';

Template.news.created = function () {
    var langCode = TAPi18n.getLanguage();
    var countSelector = {'function':'news', 'language':langCode};
    Meteor.call('Articles.count', countSelector, function(err, res) {
      if(err) {
        Session.set('numOfArticles', 0);
      } else if(res) {
        Session.set('numOfArticles', res);
      }
    });
    Session.set('numberOfElements', 5);
    Session.set('selectedPage', 1);
    Session.set('editView',null);
};

Template.news.rendered = function() {
  if($('#summernote')) $('#summernote').summernote();
};

Template.news.destroyed = function() {
  Session.set('numOfArticles', null);
  Session.set('numberOfElements', null);
  Session.set('selectedPage', null);
  Session.set('pages', null);
  Session.set('updateEditorId', null);
  $('#summernote').summernote('destroy');
};


Template.news.helpers({
  'newsArticles': function() {
    var langCode = TAPi18n.getLanguage();
    var articleFilter = {'function':'news', 'language':langCode};
    var pagenumber = Session.get('selectedPage');
    var numberOfElements = Session.get('numberOfElements');
    var elmFilter = {
        'sort': {
            'order':-1,
            'insertedDate':-1
          },
          'skip': (pagenumber - 1) * numberOfElements,
          'limit': numberOfElements
        };

    Meteor.call('Articles.count', articleFilter, function(err, res) {
      if(err) {
        console.log(err, 'Failed to count news articles');
      } else if(res) {
        //var numberOfArticles = res;
        Session.set('numOfArticles', res);
        var numberOfPages = Math.ceil(Session.get('numOfArticles') / numberOfElements);
        var pages = [];
        for (var i = 1; i <= numberOfPages; i++) {
          pages.push({'pagenumber':i});
        }
        Session.set('pages', EJSON.parse(JSON.stringify(pages)));
      }
    });
    var articles = Articles.find(articleFilter, elmFilter).fetch();
    return articles;
  },
  'languages': function() {
    return Languages.find({});
  },
  'pages': function() {
    var numberOfArticles = Session.get('numOfArticles');
    var numberOfElements = Session.get('numberOfElements');
    var numberOfPages = Math.ceil(numberOfArticles / numberOfElements);
    var pages = [];
    for (var i = 1; i <= numberOfPages; i++) {
      pages.push({'pagenumber':i});
    }
    Session.set('pages', EJSON.parse(JSON.stringify(pages)));
    return Session.get('pages');
  },
  'pageIsSelected':function(pagenumber) {
    if(pagenumber === Session.get('selectedPage')) return 'background-grey';
  },
  'moreThanZeroPages': function() {
    var numOfArticles = Session.get('numOfArticles');
    return numOfArticles !== undefined && numOfArticles !== null && numOfArticles !== 0;
  },
});

Template.news.events({
  'submit #insert-news-form': function(event, template) {
    event.preventDefault();
    var newsArticle = event.target;
    var title = newsArticle.title.value;
    var active = newsArticle.active.checked;
    var order = parseInt(newsArticle.order.value);
    var language = newsArticle.language.value;
    var html = $('#summernote').summernote('code');

    var article = {
      'title':title,
      'body':html,
      'active':active,
      'function':'news',
      'language':language,
      'order':order
    };
    Meteor.call('Articles.insert', article, function(err, res) {
      if(err) {
        Toast.error('An error occured in Articles.insert. The article was not inserted');
      } else if(res) {
        Toast.success('The article was saved successfully!');
      }
    });

    title = null;
    active = false;
    order = 0;
    language = null;
    template.$('#summernote').innerHTML = 'Write the article here';
    return false;
  },
  'click .page-elm': function(event, template) {
    Session.set('selectedPage', this.pagenumber);
  },
});

Template.newsArticle.created = function() {
  this.editActive = new ReactiveVar(false);
};

Template.newsArticle.helpers({
  'editActive': function() {
    return Template.instance().editActive.get();
  },
});

Template.newsArticle.events({
  'submit .news-form': function(event, template) {
    event.preventDefault();

    var newsArticle = event.target;
    var title = newsArticle.title.value;
    var active = newsArticle.active.checked;
    var order = parseInt(newsArticle.order.value);
    var language = newsArticle.language.value;
    var html = $('#summernote').summernote('code');

    var article = {
      'title':title,
      'body':html,
      'active':active,
      'function':'news',
      'language':language,
      'order':order
    };
    Meteor.call('Articles.insert', article, function(err, res) {
      if(err) {
        Toast.error('An error occured in Articles.insert. The article was not inserted');
      } else if(res) {
        Toast.success('The article was saved successfully!');
      }
    });

    title = null;
    active = false;
    order = 0;
    language = null;
    template.$('#summernote').innerHTML = 'Write the article here';

  },
  'click .edit-news': function(event, template) {
    template.editActive.set(!template.editActive.get());

  },
  'click .del-news': function(event, template) {
    var articleId = this._id;
    var pagesBefore = Session.get('pages');
    new Confirmation({
      message: "Are you sure you want to delete this newsarticle?",
      title: "Delete article",
      cancelText: "Cancel",
      okText: "Delete",
      success: false, // whether the button should be green or red
      focus: "Cancel" // which button to autofocus, "cancel" (default) or "ok", or "none"
    }, function (ok) {
      if(ok) {
        Meteor.call('Articles.remove', articleId, function(err, res) {
          if(err) {
            Toast.error('Failed to delete article');
            Toast.error(err);
          } else {
            Toast.success('The article was deleted successfully');
            var langCode = TAPi18n.getLanguage();
            var selector = {'function':'news', 'language':langCode};
            Meteor.call('Articles.count', selector, function(err, res) {
              if(err) {
                Session.set('numOfArticles', 0);
              } else if(res) {
                Session.set('numOfArticles', res);
                setTimeout(function() {
                  var pagesAfter = Session.get('pages');
                  var pageSelected = Session.get('selectedPage');
                  if(pagesAfter.length < pagesBefore.length && pageSelected === pagesBefore.length && pageSelected > 1)  {
                    Session.set('selectedPage', pageSelected - 1);
                  }
                }, 100);

              }
            });
          }
        });
      }
    });
  },
  'click .page-elm': function() {
    Session.set('selectedPage', this.pagenumber);
  },
});

Template.editArticle.rendered = function() {
  console.log(Template.instance().data);
  var tempData = Template.instance().data;
  var editorId = 'wysiwyg-editor-' + tempData._id._str;
  $('#' + editorId).summernote({
    'minHeight':200,
    'focus':true
  });

};

Template.editArticle.destroyed = function() {
  //if($('.wysiwyg-editor')) $('#wysiwyg-editor').summernote('destroy');
};

Template.editArticle.helpers({
  'languages': function() {
    return Languages.find({});
  },
  'langIsSelected': function(lang, langCode) {
    if(lang === langCode) return 'selected';
  },
  'isChecked': function(active) {
    if(active === true) return 'checked';
  }
});

Template.editArticle.events({
  "submit #update-news": function(event, template) {
    event.preventDefault();

    var id = this._id;
    var newsArticle = event.target;
    var title = newsArticle.title.value;
    var active = newsArticle.active.checked;
    var order = parseInt(newsArticle.order.value);
    var language = newsArticle.language.value;
    var html = $('#wysiwyg-editor-' + id._str).summernote('code');

    var article = {
      'title':title,
      'body':html,
      'active':active,
      'function':'news',
      'language':language,
      'order':order
    };
    Meteor.call('Articles.update', id, article, function(err, res) {
      if(err) {
        Toast.error('There was an error in updating article');
        Toast.error(err);
      } else {
        Toast.success('Update was successfull')
      }
    });

    return false;
  },
});
