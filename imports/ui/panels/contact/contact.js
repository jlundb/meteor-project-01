import { Template } from 'meteor/templating';
import { UserMessages } from '../../../api/usermessages.js';
import { Session } from 'meteor/session';
import { ReactiveVar } from 'meteor/reactive-var';

import './contact.html';

Template.contact.created = function() {
   if(!Session.get('messageSent')) Session.set('messageSent', false);
   Session.set('totalNumOfMessages', 0);
   Session.set('selectedPage', 1);
   Session.set('entryPerPage', 4);
   Meteor.call('UserMessages.count', {}, function(err, res) {
     if(err) {
       Session.set('totalNumOfMessages', 0);
     } else {
       Session.set('totalNumOfMessages', res);
     }
   });

};

Template.contact.destroyed = function() {
  Session.set('totalNumOfMessages', null);
  Session.set('selectedPage', null);
  Session.set('totalNumOfMessages', null);
  Session.set('pages', null);
  Session.set('entryPerPage', null);
};

Template.contact.helpers({
  'messages':function() {
    var entryPerPage = Session.get('entryPerPage');
    var selectedPage = (Session.get('selectedPage') - 1) * entryPerPage;
    var totalNumOfMessages = Session.get('totalNumOfMessages');
    var totalPages = Math.ceil(totalNumOfMessages / entryPerPage);
    var pages = [];
    for (var i = 1; i <= totalPages; i++) {
      pages.push({'pagenumber': i});
    }
    Session.set('pages', EJSON.parse(JSON.stringify(pages)));
    var selector = {};
    var options = {'skip':selectedPage, 'limit': entryPerPage, 'sort':{'submittedAt':-1}};
    return UserMessages.find(selector, options).fetch();
  },
  'messageSent':function() {
     return  Session.get('messageSent');
  },
  'completed':function() {
      return Session.get('messageSent');
  },
  'pages':function(list) {
    var totalNumOfMessages = Session.get('totalNumOfMessages');
    var entryPerPage = Session.get('entryPerPage');
    var totalPages = Math.ceil(totalNumOfMessages / entryPerPage);
    var pages = [];
    for (var i = 1; i <= totalPages; i++) {
      pages.push({'pagenumber': i});
    }
    Session.set('pages', EJSON.parse(JSON.stringify(pages)));
    return Session.get('pages');
  },
  'pageIsSelected':function(pagenumber) {
    if(pagenumber === Session.get('selectedPage')) return 'background-grey';
  },
});


Template.contact.events({
  "submit #contact-form": function(event) {
      if (Honeypot.isHuman(event.target)) {   // Process form
        Honeypot.removeHoneypotFields(event.target);
        var sendForm = event.target;
        var name = sendForm.name.value;
        var mail = sendForm.mail.value;
        var phone = sendForm.phone.value;
        var topic = sendForm.topic.value;
        var message = sendForm.message.value;
        var userMessage = {
          'name':name,
          'message':message,
          'topic':topic,
          'submittedAt': new Date()
        };
        if(mail) userMessage.mail = mail;
        if(phone) userMessage.phone = phone;

        Meteor.call('UserMessages.insert', userMessage, function(err, results) {
          if(err) {
            Toast.error('Failed to send message');
          } else if(results) {
            Toast.success('Message recieved!');
            Session.set('messageSent', true);
          }
        });
      } else { // IS SPAM
        Session.set('messageSent', true);
      }

      event.target.name.value = null;
      event.target.mail.value = null;
      event.target.phone.value = null;
      event.target.topic.value = null;
      event.target.message.value = null;
      return false;
  },
  'click .page-elm': function(event, template) {
    Session.set('selectedPage', this.pagenumber);
  },
  'click .del-message': function(event, template) {
     var messageId = this._id;
     var pagesBefore = Session.get('pages');
     new Confirmation({
       message: "Are you sure you want to delete this message?",
       title: "Delete message",
       cancelText: "Cancel",
       okText: "Delete",
       success: false, // whether the button should be green or red
       focus: "Cancel" // which button to autofocus, "cancel" (default) or "ok", or "none"
     }, function (ok) {
       if(ok) { // ok is true if the user clicked on "ok", false otherwise
         Meteor.call('UserMessages.remove', messageId, function(err, res) {
           if(err) {
             Toast.error('Deleting message failed, contact administrator');
           } else {
             Toast.success('Message deleted');
             Meteor.call('UserMessages.count', {}, function(err, res) {
               if(err) {
                 Session.set('totalNumOfMessages', 0);
               } else {
                 Session.set('totalNumOfMessages', res);
                 setTimeout(function() {
                   var pagesAfter = Session.get('pages');
                   var selectedPage = Session.get('selectedPage');
                   if(pagesAfter.length < pagesBefore.length && selectedPage === pagesBefore.length && selectedPage > 1)  {
                     Session.set('selectedPage', selectedPage - 1);
                   }
                 }, 100);
               }
             });
           }
         });
       }
    });
  }
});
