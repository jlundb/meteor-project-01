import { Template } from 'meteor/templating';
import { EJSON } from 'meteor/ejson';
import { CompanyInfo } from '../../../api/company_info.js';
import { ReactiveVar } from 'meteor/reactive-var';
import './info.html';

Template.info.helpers({
  'companyInfo': function() {
    return CompanyInfo.findOne({});
  }
});

Template.info.events({
  'submit #info-form': function(event) {
    event.preventDefault();

    var objectId = this._id;
    var data = event.target;
    var name = data.name.value;
    var postalCode = data.postalCode.value;
    var city = data.city.value;
    var address = data.address.value;
    var phone = data.phone.value;
    var mail = data.mail.value;
    var companyInfo = {
      '_id': objectId,
      'name': name,
      'postalCode':postalCode,
      'city':city,
      'address':address,
      'phone':phone,
      'mail':mail
    };

    Meteor.call('CompanyInfo.update', companyInfo, function(err, res) {
      if(err) {
        Toast.error('Failed to update company info');
        console.log(err);
      } else {
        Toast.success('Info updated successfully');
      }
    });
    return false;
  }
});
