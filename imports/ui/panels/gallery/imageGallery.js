import { Template } from 'meteor/templating';
import { Tracker } from 'meteor/tracker';
import { EJSON } from 'meteor/ejson';
import { GalleryImages } from '../../../api/gallery_images.js';
import { CategoryOptions } from '../../../api/category_options.js';
import { ReactiveVar } from 'meteor/reactive-var';
import './imageGallery.html';

Template.imageGallery.created = function() {
  Session.set('displayedCategory', 'living');
  Session.set('imageIndex', 0);
  Session.set('imageGallerySize', 0);
  Session.set('selectedPage', 1);
  Session.set('numOfElements', 6);
  Session.set('expectedElms', 6);
  var selector = {'category':Session.get('displayedCategory')};
  Meteor.call('galleryImages.count', selector, function(err, res) {
    if(err) console.log(err);
    else {
      var numOfElm = Session.get('numOfElements');
      Session.set('expectedElms', (numOfElm <= res) ? numOfElm : res);
    }
  });
};

Template.name.destroyed = function() {
  Session.set('displayedCategory', null);
  Session.set('imageIndex', null);
  Session.set('imageGallerySize', null);
  Session.set('selectedPage', null);
  Session.set('numOfElements', null);
  Session.set('pages', null);
  Session.set('expectedElms', null);
};

Template.imageGallery.helpers({
  'imageGalleryItems': function(args, args2) {
    var displayedCategory = Session.get('displayedCategory');
    var selector = {
      'category': displayedCategory ? displayedCategory : 'living'
    };
    var pagenumber = Session.get('selectedPage');
    var numOfElements = Session.get('numOfElements');
    var options = {
                      'sort':{'insertedDate':-1},
                      'skip': (pagenumber - 1) * numOfElements,
                      'limit': numOfElements
                    };
    Meteor.call('galleryImages.count', selector, function(err, res) {
      if(err) {
        console.log('Error in counting gallery images', err);
      } else if(res) {
          var numOfImages = res;
          var numOfPages = Math.ceil(numOfImages / numOfElements);
          var pages = [];
          for (var i = 1; i <= numOfPages; i++) {
            pages.push({'pagenumber':i});
          }
          Session.set('pages',EJSON.parse(JSON.stringify(pages)));
      }
    });

    var galleryItems = GalleryImages.find(selector, options).fetch();
    Session.set('imageGallerySize', galleryItems.length);
    return (galleryItems.length === Session.get('expectedElms')) ? galleryItems : false;

  },
  'getSelectedImage' : function(imageList) {
    if(imageList && imageList.length && imageList.length > 0) {
        var imageIndex = Session.get('imageIndex');
        return imageList[imageIndex];
      }
  },
  'categoryOptions': function() {
    return CategoryOptions.find({}, {'sort':{'order':1}});
  },
  'pages': function() {
      return Session.get('pages');
  },
  'pageIsSelected':function(pagenumber) {
    if(pagenumber === Session.get('selectedPage')) return 'background-grey';
  },
  'displayCategoryIsActive': function(value) {
    if(Session.get('displayedCategory') === value) return 'active';
  },
  'evalImageGalleryItems':function(imageGalleryItems) {
    return (imageGalleryItems.length >= 0);
  }

});

Template.imageGallery.events({
  'click .gallery-item': function(event, template) {
     var imageId = this._id;
     var displayedCategory = Session.get('displayedCategory');
     var galleryImagesFilter = {
       'category': displayedCategory ? displayedCategory : 'living'
     };
     var pagenumber = Session.get('selectedPage');
     var numOfElements = Session.get('numOfElements');
     var elmFilter = {
       'sort': {
         'insertedDate':-1
       },
       'skip': (pagenumber - 1) * numOfElements,
       'limit': numOfElements
     };
     var imageGalleryItems = GalleryImages.find(galleryImagesFilter, elmFilter).fetch();

     for (var i = 0; i < imageGalleryItems.length; i++) {
       if(imageGalleryItems[i]._id === imageId && Session.get('imageIndex') !== i) {
           template.$('#disp-image').fadeOut(500, function() { // FADE OUT
             Session.set('imageIndex', i);
             template.$('#disp-image').fadeIn(500); //FADE IN
           });
           return;
       }
     }

  },
  'click .del-image': function(event, template) {
    var imageId = this._id;
    var imageName = this.imageName;
    new Confirmation({
        message: 'Are you sure you want to delete "' + this.imageName  + '" ?',
        title: 'Delete "' + this.imageName + '"',
        cancelText: 'Cancel',
        okText: "Ok",
        success: false, // whether the button should be green or red
        focus: "none" // which button to autofocus, "cancel" (default) or "ok", or "none"
      }, function (ok) {
        if(ok) { // ok is true if the user clicked on "ok", false otherwise
            Meteor.call('galleryImages.remove', imageId, function(err, result) {
              if(err) {
                Toast.error('There was an error when trying to remove gallery item');
                Toast.error(err);
              } else if(result) {
                Toast.success(imageName + ' was deleted.');
                Meteor.call('galleryImages.count', selector, function(err, res) {
                  if(err) console.log(err);
                  else {
                    var numOfElm = Session.get('numOfElements');
                    Session.set('expectedElms', (numOfElm <= res) ? numOfElm : res);
                  }
                });
              }
            });
        }
    });
  },
  'click #disp-image':function(event, template) {
    Modal.show('imagePopupModal', function() {
      var displayedCategory = Session.get('displayedCategory') ? Session.get('displayedCategory') : 'living';
      var imageIndex = Session.get('imageIndex');
      if(displayedCategory && typeof imageIndex === 'number') {
          var galleryImagesFilter = {
            'category' : displayedCategory
          };
          var pagenumber = Session.get('selectedPage');
          var numOfElements = Session.get('numOfElements');
          var elmFilter = {
            'sort':{'insertedDate':-1},
            'skip': (pagenumber - 1) * numOfElements,
            'limit': numOfElements
          };
          var selectedImages = GalleryImages.find(galleryImagesFilter, elmFilter).fetch();
          return selectedImages[imageIndex];
        }
    });
  },
  'click .category-pill': function(event, template) {
    var displayCategory = Blaze.getData(event.target);
    template.$('.img-gallery-container').fadeOut(500, function() { // FADE OUT
      Session.set('displayedCategory', displayCategory.value);
      Session.set('imageIndex', 0);
      var selector = {'category':displayCategory.value};
      Meteor.call('galleryImages.count', selector, function(err, res) {
        if(err) console.log(err);
        else {
          var numOfElm = Session.get('numOfElements');
          Session.set('expectedElms', (numOfElm <= res) ? numOfElm : res);
          console.log("expectedElm", Session.get("expectedElms"));
        }
      });
      template.$('.img-gallery-container').fadeIn(500); //FADE IN
    });
    template.$('#disp-image').fadeOut(500, function() {
      template.$('#disp-image').fadeIn(500);
    });
  },
  'click .go-right' : function(event, template) {
    var imageIndex = Session.get('imageIndex');
    var imageGallerySize = Session.get('imageGallerySize');
    if(imageGallerySize && imageGallerySize > 0) {
      template.$('#disp-image').fadeOut(500, function() {
        if(imageIndex >= (imageGallerySize - 1)) {
          Session.set('imageIndex', 0);
        } else {
          Session.set('imageIndex',imageIndex + 1);
        }
        template.$('#disp-image').fadeIn(500);
      });
    }
  },
  'click .go-left' : function(event, template) {
    var imageIndex = Session.get('imageIndex');
    var imageGallerySize = Session.get('imageGallerySize');
    if(imageGallerySize && imageGallerySize > 0) {
      template.$('#disp-image').fadeOut(500, function() {
        if(imageIndex === 0) {
          Session.set('imageIndex',imageGallerySize - 1);
        } else {
          Session.set('imageIndex',imageIndex - 1);
        }
        template.$('#disp-image').fadeIn(500);
      });
    }
  },
  'click .page-elm': function(event, template) {
    var pageNumber = this.pagenumber;
    template.$('#all-images').fadeOut(500, function() {
      Session.set('imageIndex',0);
      Session.set('selectedPage', pageNumber);
      template.$('#all-images').fadeIn(500);
    });
  },
});

Template.imagePopupModal.events({
  'click .go-right' : function(event, template) {
    var imageIndex = Session.get('imageIndex');
    var imageGallerySize = Session.get('imageGallerySize');
    if(imageGallerySize && imageGallerySize > 0) {
      template.$('.popup-image').fadeOut(500, function() {
        if(imageIndex >= (imageGallerySize - 1)) {
          Session.set('imageIndex', 0);
        } else {
          Session.set('imageIndex',imageIndex + 1);
        }
        template.$('.popup-image').fadeIn(500);
      });
    }
  },
  'click .go-left' : function(event, template) {
    var imageIndex = Session.get('imageIndex');
    var imageGallerySize = Session.get('imageGallerySize');
    if(imageGallerySize && imageGallerySize > 0) {
      template.$('.popup-image').fadeOut(500, function() {
        if(imageIndex === 0) {
          Session.set('imageIndex',imageGallerySize - 1);
        } else {
          Session.set('imageIndex',imageIndex - 1);
        }
        template.$('.popup-image').fadeIn(500);
      });
    }
  },
});
