import { Template } from 'meteor/templating';
import { CategoryOptions } from '../../../api/category_options.js';
import { ReactiveVar } from 'meteor/reactive-var';
import './imageGallery.js';
import './gallery.html';

Template.gallery.created = function() {
  Session.set('selectedCategory', 'living');
  this.fileEncoding = new ReactiveVar();
  this.fileName = new ReactiveVar();
};

Template.gallery.rendered = function() {
  var self = this;
	this.autorun(function(a) {
	    var data = Template.currentData(self.view);
			if(!data) return; // no data still waiting...
					// do stuff with data, it's available now...
					// data is an object containing sub1 & sub2 - both reactive collections
					//

	       });

};

Template.gallery.helpers({
  'imageFileUploaded': function() {
    if(Session.get('imageUploaded')) {
      return Session.get('imageUploaded');
    } else {
      Session.set('imageUploaded', false);
      return Session.get('imageUploaded');
    }
  },
  'fileName': function() {
    var fileName = Template.instance().fileName.get();
    return fileName ? fileName : null;
  },
  'categoryOptions': function() {
    return Template.currentData(this.view).categoryOptions;
  },
  'isSelected': function(selected) {
    return (Session.get('selectedCategory') === selected) ? 'selected' : '';
  },
  'galleryItems': function() {
    return Template.currentData(this.view).galleryItems;
  }
});

Template.gallery.events({
  "submit #file-upload-form": function(event, template) {
    event.preventDefault();
    var name = event.target.imageName.value;
    var desc = event.target.imageDesc.value;
    var category = event.target.imageCategorySelect.value;
    var template = Template.instance();
    var image = {
      'name': name,
      'desc': desc
    };
    var galleryImage = {
      'imageName': name,
      'imageDescription': desc,
      'category': category,
      'encoding' : template.fileEncoding.get()
    };

    Toast.info('Upload started');
    Meteor.call('galleryImages.simpleInsert', galleryImage, function(err, res) {
      if(err) Toast.error('Gallery image insert failed');
      else Toast.success('Gallery image was inserted.');
    });

    if(false)
    Meteor.call('galleryImages.insert', galleryImage, image, function(err, res) {
      if(err) Toast.error('Gallery image insert failed');
      else Toast.success('Gallery image was inserted.');
    });

    event.target.imageName.value = null;
    event.target.imageDesc.value = null;
    event.target.imageFile.value = null;
    template.fileEncoding.set(null);
    template.fileName.set(null);

  },
  'change input[type=file]': function(event, template) {
    var files = event.target.files;
    if(files.length === 0) return;
    var file = files[0];
    template.fileName.set(file.name);
    var fileReader = new FileReader();
    fileReader.onload = function(event) {
      var fileEncoding = event.target.result;
      template.fileEncoding.set(fileEncoding);
    };
    fileReader.readAsDataURL(file);
  },
});
