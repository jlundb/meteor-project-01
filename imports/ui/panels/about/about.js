import { Template } from 'meteor/templating';
import { Articles } from '../../../api/articles.js';
import { ArticleImages } from '../../../api/article_images.js';
import { Images } from '../../../api/images.js';
import { Languages } from '../../../api/languages.js';
import { ReactiveVar } from 'meteor/reactive-var';
import { Tracker } from 'meteor/tracker';
import './about.html';

Template.about.helpers({
  'article': function() {
    var langCode = TAPi18n.getLanguage();
    var selector = {'function':'about', 'language': langCode};
    return Articles.findOne(selector);
  },
  'articleImage': function() {
    return ArticleImages.findOne({});
  },
  'isClickable':function() {
      return Meteor.userId() ? 'clickable' : '';
  },
  'aboutUsReady':function(article, articleImage) {
    return (article && articleImage);
  }
});

Template.about.events({
  'click #edit-about': function(event, template){
    var id = this._id
    if(Meteor.userId()) {
      Modal.show('aboutModal', function() {
        return Articles.findOne({'_id':id});
      });
    }
  },
  'click #about-image': function(event, template) {
    if(Meteor.userId()) {
      Modal.show('imageModal', function() {
        return ArticleImages.findOne({'function':'about'});
      });
    }
  },
});

Template.aboutModal.rendered = function() {
  if($('#wysiwyg-editor')) $('#wysiwyg-editor').summernote();
};

Template.aboutModal.destroyed = function() {
  if($('#wysiwyg-editor')) $('#wysiwyg-editor').summernote('destroy');
};

Template.aboutModal.helpers({
  'languages': function() {
    return Languages.find({});
  },
  'langIsSelected': function(lang, langCode) {
    if(lang === langCode) return 'selected';
  },
  'isChecked': function(active) {
    if(active === true) return 'checked';
  }
});

Template.aboutModal.events({
  "submit #update-news": function(event, template) {
    event.preventDefault();
    var id = this._id;
    var newsArticle = event.target;
    var title = newsArticle.title.value;
    var active = newsArticle.active.checked;
    var order = parseInt(newsArticle.order.value);
    var language = newsArticle.language.value;
    var html = $('#wysiwyg-editor').summernote('code');

    var article = {
      'title':title,
      'body':html,
      'active':active,
      'function':'about',
      'language':language,
      'order':order
    };
    Meteor.call('Articles.update', id, article, function(err, res) {
      if(err) {
        Toast.error('There was an error in updating article');
        Toast.error(err);
      } else {
        Toast.success('Update was successfull')
      }
    });

    return false;
  },
});

Template.imageModal.created = function() {
  this.imagePerPage = new ReactiveVar(9);
  this.selectedPage = new ReactiveVar(1);
  this.pages = new ReactiveVar();
  this.imageGallerySize = new ReactiveVar();
  this.fileEncoding = new ReactiveVar();
  this.fileName = new ReactiveVar();
  var imageGallerySize = this.imageGallerySize;
  Meteor.call('images.count', {}, function(err, res) {
    if(err) {
      Toast.error('Failed to count images');
      imageGallerySize.set(0);
    } else if(res) {
      imageGallerySize.set(res);
    }
  });
};

Template.imageModal.helpers({
  'images': function() {
    var selectedPage = Template.instance().selectedPage.get();
    var imagePerPage = Template.instance().imagePerPage.get();
    return Images.find({}, {'skip':(selectedPage - 1) * imagePerPage, 'limit':imagePerPage});
  },
  'pages':function(list) {
    var totalNumOfMessages = Template.instance().imageGallerySize.get();
    var entryPerPage = Template.instance().imagePerPage.get();
    var totalPages = Math.ceil(totalNumOfMessages / entryPerPage);
    var pages = [];
    for (var i = 1; i <= totalPages; i++) {
      pages.push({'pagenumber': i});
    }
    Template.instance().pages.set(EJSON.parse(JSON.stringify(pages)));
    return Template.instance().pages.get();
  },
  'pageIsSelected':function(pagenumber) {
    if(pagenumber === Template.instance().selectedPage.get()) return 'background-grey';
  },
  'fileName': function() {
    var fileName = Template.instance().fileName.get();
    return fileName ? fileName : null;
  },
});

Template.imageModal.events({
  "submit #file-upload-form": function(event, template) {
    event.preventDefault();
    var name = event.target.imageName.value;
    var template = Template.instance();
    var image = {
      'name': name,
      'encoding' : template.fileEncoding.get()
    };

    Toast.info('Upload started');
    Meteor.call('images.insert', image, function(err, res) {
      if(err) {
        Toast.error('An error occured during image upload');
        Toast.error(err);
      } else if(res) {
        Toast.success('The file was uploaded');
      }
    });

    event.target.imageName.value = null;
    event.target.imageFile.value = null;
    template.fileEncoding.set(null);
    template.fileName.set(null);
    return false;
  },
  'change input[type=file]': function(event, template) {
    var files = event.target.files;
    if(files.length === 0) return;
    var file = files[0];
    template.fileName.set(file.name);
    var fileReader = new FileReader();
    fileReader.onload = function(event) {
      var fileEncoding = event.target.result;
      template.fileEncoding.set(fileEncoding);
    };
    fileReader.readAsDataURL(file);
  },
  'click .img-elm': function(event, template) {
    var image = this;
    var articleImage = template.data;
    articleImage.imageId = image._id;
    Meteor.call('ArticlesImages.update', articleImage, function(err, res) {
      if(err) {
        Toast.error('Failed to update image');
        Toast.error(err);
      } else if(res) {
        Toast.success('Images was updated');
      }
    });
  },
  'click .page-elm': function(event, template) {
    template.selectedPage.set(this.pagenumber);
  },
  'click .del-image':function(event, template) {
    var imageId = this._id;
    var pagesBefore = template.pages.get();

    new Confirmation({
      message: "Are you sure you want to delete this image?",
      title: "Delete image",
      cancelText: "Cancel",
      okText: "Delete",
      success: false, // whether the button should be green or red
      focus: "none" // which button to autofocus, "cancel" (default) or "ok", or "none"
    }, function (ok) {
      if(ok) {
        Meteor.call('images.remove', imageId, function(err, res) {
          if(err) {
            Toast.error('Failed to delete image');
            Toast.error(err);
          } else {
            Toast.success('The image was deleted successfully');
            Meteor.call('images.count', {}, function(err, res) {
              if(err) {
                Toast.error('Failed to count images');
                template.imageGallerySize.set(0);
              } else if(res) {
                template.imageGallerySize.set(res);
                setTimeout(function() {
                  var pagesAfter = template.pages.get();
                  var pageSelected = template.selectedPage.get();
                  if(pagesAfter.length < pagesBefore.length && pageSelected === pagesBefore.length && pageSelected > 1)  {
                    template.selectedPage.set(template.pages.get() - 1);
                  }
                }, 100);
              }
            });
          }
        });
      }
    });
  },
});
