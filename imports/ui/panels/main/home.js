import { Articles } from '../../../api/articles.js';
import { Blaze } from 'meteor/blaze';
import { Template } from 'meteor/templating';
import { Tracker } from 'meteor/tracker';
import { Languages } from '../../../api/languages.js';
import { CarouselImages } from '../../../api/carousel_images.js';

import './carousel.js';
import './home.html';

Template.home.created = function() {
  this.fileEncoding = new ReactiveVar();
  this.fileName = new ReactiveVar();
  this.dragData = new ReactiveVar();
};

Template.home.helpers({
  'articles': function() {
    var langCode = TAPi18n.getLanguage();

    return Articles.find({'function':'home', 'language':langCode});
  },
  'carouselItems': function() {
    return CarouselImages.find({}, {'sort':{'slideNr':1}});
  },
  'fileName': function() {
    return Template.instance().fileName.get();
  },
});

Template.home.events({
  'click .art-edit':function() {
    var context = this;
    if(Meteor.userId()) {
      Modal.show('homeModal', function() {
        return context;
      });
    }
  },
  "submit #file-upload-form": function(event, template) {
    event.preventDefault();
    var name = event.target.imageName.value;
    var template = Template.instance();
    var carouselImage = {
      'name': name,
      'encoding' : template.fileEncoding.get()
    };

    Meteor.call('CarouselImages.maxSlideNr',{}, function(err, slideNr) {
      if(err) {
        Toast.error('Failed to get maxSlideNr');
        console.log(err);
      } else if(typeof slideNr === 'number') {
        carouselImage.slideNr = (slideNr === -1) ? 0 : (slideNr + 1);

        Meteor.call('CarouselImages.insert',carouselImage, function(err1, res) {
          if(err1) {
            Toast.error('Failed to insert Carousel Image');
            console.log(err1);
          } else if(res) {
            Toast.success('Carousel image inserted successfully');
          }
        });
      }
    });
    event.target.imageName.value = null;
    template.fileEncoding.set(null);
    template.fileName.set(null);

  },
  'change input[type=file]': function(event, template) {
    var files = event.target.files;
    if(files.length === 0) return;
    var file = files[0];
    template.fileName.set(file.name);
    var fileReader = new FileReader();
    fileReader.onload = function(event) {
      var fileEncoding = event.target.result;
      template.fileEncoding.set(fileEncoding);
    };
    fileReader.readAsDataURL(file);
  },
  'dragstart .img-drag': function(event, template) {
    template.dragData.set(EJSON.parse(JSON.stringify(Blaze.getData(event.target))));
    console.log(Blaze.getData(event.target));
  },
  'drop .carousel-container' : function(event, template) {
    event.preventDefault();
    var dropData = template.dragData.get();
    dropData._id = new Meteor.Collection.ObjectID(dropData._id._str);
    var currData = Blaze.getData(event.target);

    if(dropData.slideNr !== currData.slideNr) {
      Meteor.call('CarouselImages.swapPosition', dropData, currData, function(err) {
        if(err) console.log(err);
      });
    }
  },
  'dragover .carousel-container' : function(event, template) {
    event.preventDefault();
  },
  'click .del-item':function(event, target) {
    var carouselImage = this;
    new Confirmation({
      message: "Are you sure you want to delete this image?",
      title: "Delete carousel image",
      cancelText: "Cancel",
      okText: "Delete",
      success: false, // whether the button should be green or red
      focus: "none" // which button to autofocus, "cancel" (default) or "ok", or "none"
    }, function (ok) {
      if(ok) { // ok is true if the user clicked on "ok", false otherwise
        Meteor.call('CarouselImages.remove',carouselImage, function(err, res) {
          if(err) console.log(err);
          else Toast.success('Image deleted successfully');
        });
      }
   });
  }
});


Template.homeModal.rendered = function() {
  if($('#wysiwyg-editor')) $('#wysiwyg-editor').summernote();
};

Template.homeModal.destroyed = function() {
  if($('#wysiwyg-editor')) $('#wysiwyg-editor').summernote('destroy');
};

Template.homeModal.helpers({
  'languages': function() {
    return Languages.find({});
  },
  'langIsSelected': function(lang, langCode) {
    if(lang === langCode) return 'selected';
  },
  'isChecked': function(active) {
    if(active === true) return 'checked';
  }
});

Template.homeModal.events({
  "submit #update-news": function(event, template) {
    event.preventDefault();
    var id = this._id;
    var newsArticle = event.target;
    var title = newsArticle.title.value;
    var active = newsArticle.active.checked;
    var order = parseInt(newsArticle.order.value);
    var language = newsArticle.language.value;
    var html = $('#wysiwyg-editor').summernote('code');

    var article = {
      'title':title,
      'body':html,
      'active':active,
      'function':'home',
      'language':language,
      'order':order
    };
    Meteor.call('Articles.update', id, article, function(err, res) {
      if(err) {
        Toast.error('There was an error in updating article');
        Toast.error(err);
      } else {
        Toast.success('Update was successfull')
      }
    });

    return false;
  },
});
