import { Template } from 'meteor/templating';
import { CarouselImages } from '../../../api/carousel_images.js';

import './carousel.html';

Template.carousel.created = function() {
  this.fileEncoding = new ReactiveVar();
  this.fileName = new ReactiveVar();
};

Template.carousel.helpers({
  'carouselItems': function() {
    var carouselImages = CarouselImages.find({}).fetch();
    /*
    var defaultImages = [
      {'encoding':'images/carousel/furniture_1.jpg','slideNr':0},
      {'encoding':'images/carousel/furniture_2.jpg','slideNr':1},
      {'encoding':'images/carousel/furniture_3.jpg','slideNr':2}
    ];
    return carouselImages.length ? carouselImages : defaultImages;
    */
    return carouselImages;
  },
  'fileName': function() {
    return Template.instance().fileName.get();
  },
});

Template.carousel.events({
  "submit #file-upload-form": function(event, template) {
    event.preventDefault();
    var name = event.target.imageName.value;
    var template = Template.instance();

    var carouselImage = {
      'name': name,
      'encoding' : template.fileEncoding.get()
    };
    console.log('before method');
    console.log(carouselImage);

    Meteor.call('CarouselImages.maxSlideNr',{}, function(err, slideNr) {
      if(err) {
        Toast.error('Failed to get maxSlideNr');
        console.log(err);
      } else if(typeof slideNr === 'number') {
        console.log("res: ", slideNr);
        carouselImage.slideNr = (slideNr === -1) ? 0 : (slideNr + 1);
        console.log(carouselImage);

        Meteor.call('CarouselImages.insert',carouselImage, function(err1, res) {
          if(err1) {
            Toast.error('Failed to insert Carousel Image');
            console.log(err1);
          } else if(res) {
            Toast.success('Carousel image inserted successfully');
          }
        });
      }
    });
    event.target.imageName.value = null;
    template.fileEncoding.set(null);
    template.fileName.set(null);

  },
  'change input[type=file]': function(event, template) {
    var files = event.target.files;
    if(files.length === 0) return;
    var file = files[0];
    template.fileName.set(file.name);
    var fileReader = new FileReader();
    fileReader.onload = function(event) {
      var fileEncoding = event.target.result;
      template.fileEncoding.set(fileEncoding);
    };
    fileReader.readAsDataURL(file);
  },
});

Template.carouselItem.helpers({
  'getLiClass':function(slideNr) {
      return slideNr === 0 ? 'active' : '';
  },
});

Template.carouselImage.helpers({
  'getDivClass':function(slideNr) {
      return slideNr === 0 ? 'item active' : 'item';
  },
});
