import '/imports/ui/panels/about/about.js';
import '/imports/ui/panels/contact/contact.js';
import '/imports/ui/panels/gallery/gallery.js';
import '/imports/ui/panels/info/info.js';
import '/imports/ui/panels/login/login.js';
import '/imports/ui/loading/loadingSpinner.js';
import '/imports/ui/utility/loading.js';
import '/imports/ui/panels/main/home.js';
import '/imports/ui/panels/news/news.js';
import '/imports/ui/not_found/not_found.js';
import { Routes } from '/imports/api/routes.js';
import { Meteor } from 'meteor/meteor';
import { CategoryOptions } from '../imports/api/category_options.js';
import { CarouselImages } from '../imports/api/carousel_images.js';

Router.configure({
  layoutTemplate: 'layout',
  notFoundTemplate: 'notFound',
  loadingTemplate: 'loadingSpinner'
});

Router.route('/', {
  'waitOn': function() {
    return [
      function() { return Meteor.subscribe('articles', 'home'); },
      function() { return Meteor.subscribe('languages'); },
      function() { return Meteor.subscribe('carousel_images'); }
    ];
  },
  'onBeforeAction': function() {
    this.next();
  },
  'onAfterAction':function() {
    if(document.body) document.body.scrollIntoView();
  },
  'action':function() {
    if(this.ready()) {
      this.render('home');
      $('#content-body').hide(0, function() {
        $('#content-body').fadeIn(1100);
      });
    }
    else this.render('loadingSpinner');
  },
});

Router.route('/about', {
  'waitOn': function() {
    return [
      function() { return Meteor.subscribe('article_images', 'about'); },
      function() { return Meteor.subscribe('images'); },
      function() { return Meteor.subscribe('languages'); },
      function() { return Meteor.subscribe('articles', 'about'); }
    ];
  },
  'onBeforeAction': function() {
    this.next();
  },
  'onAfterAction':function() {
    if(document.body) document.body.scrollIntoView();
  },
  'action':function() {
    if(this.ready()) {
      this.render('about');
      $('#content-body').hide(0, function() {
        $('#content-body').fadeIn(1100);
      });

    }
    else {
      this.render('loadingSpinner');
    }
  }
});

Router.route('/contact', {
  'waitOn': function() {
    return [
      function() { return Meteor.subscribe('usermessages'); }
    ];
  },
  'onBeforeAction': function() {
    this.next();
  },
  'onAfterAction':function() {
    if(document.body) document.body.scrollIntoView();
  },
  'action':function() {
    if(this.ready()) {
      this.render('contact');
      $('#content-body').hide(0, function() {
        $('#content-body').fadeIn(1100);
      });
    }
    else this.render('loadingSpinner');
  }
});

Router.route('/news', {
  'waitOn': function() {
    return [
      function() { return Meteor.subscribe('articles', 'news'); },
      function() { return Meteor.subscribe('languages'); }
    ];
  },
  'onBeforeAction': function() {
    this.next();
  },
  'onAfterAction':function() {
    if(document.body) document.body.scrollIntoView();
  },
  'action':function() {
    if(this.ready()) {
      this.render('news');
      $('#content-body').hide(0, function() {
        $('#content-body').fadeIn(1100);
      });
    }
    else this.render('loadingSpinner');
  }
});

Router.route('/login', {
  'onAfterAction':function() {
    if(document.body) document.body.scrollIntoView();
  },
  'action':function() {
    if(this.ready()) this.render('korntinLogin');
    else this.render('loadingSpinner');
  },
});

Router.route('/gallery', {
  'waitOn': function() {
    return [
      function() { return Meteor.subscribe('category_options'); },
      function() { return Meteor.subscribe('gallery_images'); },
      function() { return Meteor.subscribe('images'); }
    ];
  },
  'onBeforeAction': function() {
    this.next();
  },
  'onAfterAction':function() {
    if(document.body) document.body.scrollIntoView();
  },
  'action':function() {
    if(this.ready()) {
      this.render('gallery');
      $('#content-body').hide(0, function() {
        $('#content-body').fadeIn(1100);
      });
    }
    else this.render('loadingSpinner');
  },
  'data': function() {
       return {
         'categoryOptions':CategoryOptions.find().fetch()
       }
  },
});

Router.route('/info', {
  'waitOn': function() {
    return [
      function() { return Meteor.subscribe('company_info'); }
    ];
  },
  'onAfterAction':function() {
    if(document.body) document.body.scrollIntoView();
  },
  'action':function() {
    if(this.ready()) this.render(Meteor.userId() ? 'info' : 'notFound');
    else this.render('loadingSpinner');
  },
});
