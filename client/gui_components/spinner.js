Meteor.Spinner.options = {
    lines: 12, // The number of lines to draw
    length: 20, // The length of each line
    width: 8, // The line thickness
    radius: 15, // The radius of the inner circle
    corners: 0.9, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: 'rgb(204, 0, 0)', // #rgb or #rrggbb
    speed: 0.7, // Rounds per second
    trail: 60, // Afterglow percentage
    shadow: true, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9
};
