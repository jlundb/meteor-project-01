Toast.options = {
  closeButton: false,
  progressBar: false,
  positionClass: 'toast-top-full-width',
  showEasing: 'swing',
  hideEasing: 'linear',
  showMethod: 'fadeIn',
  hideMethod: 'fadeOut',
  timeOut: 3000,
};
