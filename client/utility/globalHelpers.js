import { Template } from 'meteor/templating';
import { Images } from '../../imports/api/images.js';

Template.registerHelper('formatDate', function(date, formating) {
  return moment(date).format(formating);
});

Template.registerHelper("getImage", function(imageId) {
    var image = Images.findOne({'_id':imageId});
    return image ? image.encoding : 'images/loading/gears.svg';
});
