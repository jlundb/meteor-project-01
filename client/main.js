import './utility/globalHelpers.js';
import '../imports/startup/accounts-config.js';
import '../imports/ui/body.js';

Meteor.startup(() => {
  TAPi18n.setLanguage('no').done(function () {}).fail(function (error_message) {
    console.log(error_message);
  });
});
